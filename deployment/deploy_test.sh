version=$1
cd ~/PycharmProjects/web_ubunibas_forms/;
/home/martin/python_venvs/venv/bin/python3 setup.py sdist;
scp dist/ubunibas_forms-${version}.tar.gz web_services@ub-webproxy.ub.unibas.ch:~/venv_test/web_packages/
ssh web_services@ub-webproxy.ub.unibas.ch "cd ~/venv_test/web_packages/; tar xzf ubunibas_forms-${version}.tar.gz; cd ubunibas_forms-${version}; ~/venv_test/bin/python3 setup.py install"

