import os
import json
import socket
import pickle
import sqlite3
import requests
import datetime
from xml.etree import ElementTree

from flask import Flask, jsonify, Blueprint, request, Response, redirect, render_template, current_app
from flask_login import LoginManager, login_required, current_user, logout_user, login_user, UserMixin
from flask_restx import Namespace, Resource, fields

from flask_wtf import FlaskForm

from wtforms import BooleanField, StringField, PasswordField, validators, \
    SubmitField, SelectField, RadioField, TextAreaField, ValidationError, HiddenField
from wtforms.widgets import PasswordInput

from jwkest.jwk import import_rsa_key

from oic.utils.keyio import KeyBundle
from oic.oic import Client, RegistrationResponse, rndstr
from oic.utils.authn.client import CLIENT_AUTHN_METHOD
from oic.utils.http_util import Redirect
from oic.oic.message import AuthorizationResponse


ub_auth = Blueprint('ub_auth', __name__)

# TODO: Redirect über cookie lösen?
# TODO: user in DB nach AUth Methode unterscheiden
# TODO: revoke token um sich auszuloggen
# TODo: api keys alma in config

def check_firmenkund(form, field):
    user_data = UBUnibasAlmaLogin.get_alma_firmen_user(field.data, form.slsp_password.data)
    """12 Swiss Library
    14 Commercial Organization
    15  Non-profit Organization
    92 Special user cantonal library"""

    if user_data and user_data.get("slsp_usergroup_hidden") in ["12", "14", "15", "92"]:
        uid = field.data
        login_user(AuthUserObject(uid, user_data))
        pass
    elif not user_data:
        raise ValidationError(
            'Ihre Anmeldedaten stimmen nicht für {}.'.format(field.data))
    else:
        raise ValidationError(
            'Ihr Account ist kein Firmenaccount, sondern ein {}.'.format(user_data.get("slsp_usergroup_desc_hidden") or "unbekannter Account-Type"))


class AuthChoice(FlaskForm):
    form_title = "Authentifizierungs-Methode"
    oidc = SubmitField('Anmeldung mit Switch EDU-ID')
    slsp = SubmitField('Anmeldung mit institutionellem Account swisscovery')


class SLSPFirmenLogin(FlaskForm):
    man_benutzernummer = StringField('Login',
                                     [validators.required(message="Feld muss ausgefüllt sein"),
                                      check_firmenkund])
    slsp_password = StringField('Passwort', [validators.required(message="Feld muss ausgefüllt sein")],
                                       widget=PasswordInput(hide_value=False))
    state_hidden = StringField('redirect_state')
    submit = SubmitField('Anmelden')
    logout = SubmitField('Zurück')

@ub_auth.route('/sector_id', methods=['GET'])
def get_sector_id():
    return jsonify(["https://ub-test-webform.ub.unibas.ch/login/oidc",
            "https://ub-webform.ub.unibas.ch/login/oidc",
            "https://ub-webform2.ub.unibas.ch/login/oidc",
            ])

@ub_auth.route('/login', methods=['GET', 'POST'])
def login():
    auth_choice = AuthChoice(request.form)
    orig_redirect = request.args.get("next") or ""
    print("REDIRECT", orig_redirect )
    if current_user.is_authenticated:
        return redirect(orig_redirect or "/userinfo")
    elif request.method == 'POST' and not request.form.get("logout"):
        if auth_choice.oidc.data:
            auth_method = SwitchOIDCKey(orig=orig_redirect)
            return auth_method.redirect()
        elif auth_choice.slsp.data:
            redirect_state = rndstr()
            SwitchOIDCKey.set_redirect(redirect_state, orig_redirect)
            auth_form = SLSPFirmenLogin()
            auth_form.process(formdata=request.form, obj={}, **{"state_hidden": redirect_state})

            return render_template("auth_form_temp.html", title="Anmeldung institutioneller Account swisscovery",
                                   form=auth_form, eduid_service=None, user=None,
                                   remove_empty_fields=True, status="fillout", header=True,
                                   specific_parts={})
        else:
            auth_form = SLSPFirmenLogin(request.form)
            if not auth_form.validate():
                return render_template("auth_form_temp.html", title="Anmeldung institutioneller Account swisscovery",
                                       form=auth_form, eduid_service=None, user=None,
                                       remove_empty_fields=True, status="fillout", header=True,
                                       specific_parts={})
            else:
                state = auth_form.state_hidden.data
                login_redirect = orig_redirect or SwitchOIDCKey.get_redirect(state) or ""
                return redirect(login_redirect)

    else:
        return render_template("auth_choice.html", title="Authentifizierungs-Methode",
                               form=AuthChoice(), eduid_service=None, user=None,
                               remove_empty_fields=True, status="auth_method", header=True,
                               specific_parts={})


@ub_auth.route('/login/oidc', methods=['GET'])
def oidc_login():
    code_args = json.dumps(dict(request.args))
    oic_auth_user = SwitchOIDCKey()
    user_object = oic_auth_user.get_userinfo_secret(code_args)
    if not user_object:
        return render_template(template_name_or_list="message_template.html", message="Ihr SWITCH edu-ID-Konto muss vor der Bestellung noch mit einem swisscovery-Benutzungskonto verknüpft werden. "
                                                                                      "Melden Sie sich dazu auf der Registrierungsseite an und ergänzen Sie die noch fehlenden Angaben: "
                                                                                      "<a href='https://registration.slsp.ch/?iz=ubs'>https://registration.slsp.ch/?iz=ubs</a>",
                           header=False, error=True)
    login_user(user_object)
    return oic_auth_user.orig_redirect()

@ub_auth.route('/logout')
def logout():
    logout_user()
    return render_template(template_name_or_list="message_template.html", message="Sie haben sich erfolgreich ausgeloggt.",
                           header=False, error=False)

@ub_auth.route("/userinfo")
@login_required
def settings():
    return render_template(template_name_or_list="message_template.html", message="You are logged in as {}".format(str(current_user.get_id())),
                           header=False, error=False)


class AuthUserObject():

    def __init__(self, uid=None, userinfo={}):
        self.uid = uid
        self.userinfo = userinfo

    @property
    def is_anonymous(self):
        return False

    @property
    def is_authenticated(self):
        if self.userinfo:
            return True

    @property
    def is_active(self):
        return True

    def get_id(self):
        return self.uid

class SwitchOIDCKey(UserMixin):

    def __init__(self, orig=None):
        self.client = self.build_oic_client()
        session = {"state": rndstr(), "nonce": rndstr()}
        oidc_args = {
            "client_id": current_app.config["CLIENT_ID"],
            "redirect_uri": self.client.registration_response["redirect_uris"][0],
            "scope": current_app.config["OIC_SCOPE"]
        }
        oidc_args.update(session)
        self.set_redirect(session["state"], orig)
        auth_req = self.client.construct_AuthorizationRequest(request_args=oidc_args)

        self.login_url = auth_req.request(self.client.authorization_endpoint)
        self.next = orig

    def build_oic_client(self):

        oic_client = Client(client_authn_method=CLIENT_AUTHN_METHOD)
        oic_client.provider_config(current_app.config["OIC_PROVIDER"])
        info = {"client_id": current_app.config["CLIENT_ID"],
                "redirect_uris": current_app.config["OIDC_REDIRECT_URIS"]}
        if current_app.config.get("CLIENT_SECRET"):
            info.update({"client_secret": current_app.config["CLIENT_SECRET"]})
        client_reg = RegistrationResponse(**info)
        oic_client.store_registration_info(client_reg)
        return oic_client

    def redirect(self):
        return Redirect(self.login_url)

    def orig_redirect(self):
        return redirect(self.redirect_url)

    def get_userinfo(self, state):
        user_data = dict(self.client.do_user_info_request(state=state))
        uid = user_data.get("swissEduPersonUniqueID")
        slsp_data = UBUnibasAlmaLogin.get_alma_user(uid)
        print(user_data)
        if uid and slsp_data:
            conn = sqlite3.connect(current_app.config["AUTH_DB"])
            c = conn.cursor()
            user_data = json.dumps(user_data)
            c.execute("INSERT OR REPLACE INTO auth_users VALUES (?, ?)", [uid, user_data])
            conn.commit()
            conn.close()
        else:
            return False, user_data
        print("TEST",uid, user_data)
        return uid, user_data

    def get_userinfo_secret2(self, code_args):
        try:
            aresp = self.client.parse_response(AuthorizationResponse, info=code_args)
            args = {
                "client_id": self.client.client_id,
                "code": aresp["code"],
                "redirect_uri": self.client.registration_response["redirect_uris"][0],
            }
            access_token = self.client.do_access_token_request(state=aresp["state"], request_args=args,
                                                       authn_method="client_secret_basic")

            """import requests
            logout_data = {"token": access_token.get("access_token")}
            logout_data.update(args)
            test = requests.post("https://login.test.eduid.ch/idp/profile/oauth2/revocation", data=logout_data)
            print(test)"""
            self.state = aresp["state"]
            uid, user_data = self.get_userinfo(self.state)
            if uid:
                self.redirect_url = self.get_redirect(self.state) or "/login"
                return AuthUserObject(uid, user_data)
            else:
                print("No User Data for state", self.state, uid, user_data)
                return False
        except Exception:
            import traceback
            traceback.print_exc()
            return False

    def make_assertion(self, lifetime=60):
        import jwt
        pub_key = open("/home/martin/PycharmProjects/ubunibas_forms/oidc_auth/keys/key_pub.pem").read()
        priv_key = open("/home/martin/PycharmProjects/ubunibas_forms/oidc_auth/keys/key_priv.pem").read()

        _alg = "RS256"
        from oic.utils.time_util import utc_time_sans_frac
        from jwkest.jwt import b64encode_item
        _now = utc_time_sans_frac()
        payload = dict(
            iss="ub-test-unibas-webform-key",
            sub="ub-test-unibas-webform-key",
            aud="https://login.test.eduid.ch/idp/profile/oidc/token",
            jti=rndstr(32),
            exp=_now,
            iat=_now + lifetime
        )
        encoded_jwt = jwt.encode(payload, priv_key, algorithm="RS256", headers={"alg": "RS256"})
        encoded_jwtb64 = b64encode_item(encoded_jwt).decode("utf-8")
        print(encoded_jwt.decode("utf-8"))
        return encoded_jwt.decode("utf-8")

    def get_userinfo_secret(self, code_args):
        aresp = self.client.parse_response(AuthorizationResponse, info=code_args)
        args = {
            "client_id": self.client.client_id,
            "code": aresp["code"],
            "redirect_uri": self.client.registration_response["redirect_uris"][0],
        }
        _key = import_rsa_key(current_app.config["PRIV_KEY_JWT"])
        kc_rsa = KeyBundle(
            [
                {"key": _key, "kty": "RSA", "use": "ver"},
                {"key": _key, "kty": "RSA", "use": "sig"},
            ]
        )
        self.client.keyjar[""] = kc_rsa
        access_token = self.client.do_access_token_request(state=aresp["state"], request_args=args, algorithm="RS256",
                                               authn_endpoint= "token", authn_method="private_key_jwt")#, client_assertion=self.make_assertion())
        print(access_token)
        self.state = aresp["state"]
        uid, user_data = self.get_userinfo(self.state)
        if uid:
            print(self.get_redirect(self.state))
            print(self.state)
            self.redirect_url = self.get_redirect(self.state) or "/login"
            return AuthUserObject(uid, user_data)
        else:
            print("No User Data for state", self.state, uid, user_data)
            return False

    @staticmethod
    def get_redirect(state):
        conn = sqlite3.connect(current_app.config["AUTH_DB"])
        c = conn.cursor()
        c.execute('SELECT redirect FROM redirect WHERE state=?', [state])
        redirect = c.fetchone()
        conn.close()
        return redirect[0]

    @staticmethod
    def set_redirect(state, redirect):
        conn = sqlite3.connect(current_app.config["AUTH_DB"])
        c = conn.cursor()
        c.execute("INSERT INTO redirect VALUES (?, ?)", [state, redirect])
        conn.commit()
        conn.close()

    @property
    def is_authenticated(self):
        if self.xml_login_response and self.is_active:

            # print(pickeled_auth_user)
            if not self.get_id():
                conn = sqlite3.connect(current_app.config["AUTH_DB"])
                c = conn.cursor()
                pickeled_auth_user = pickle.dumps(self)
                c.execute("INSERT INTO auth_users VALUES (?, ?)", [self.uid, pickeled_auth_user])
                conn.commit()
                conn.close()
            return True
        else:
            return False

class UBUnibasAlmaLogin(UserMixin):

    def __init__(self, uid, pwd):
        self.uid = uid
        self.pwd = pwd

    @staticmethod
    def get_alma_firmen_user(user_bib_num, pwd):
        user = {}
        # user_bib_num = "martin.reisacher@unibas.ch"
        # user_bib_num = "mbs01130962"
        if not user_bib_num:
            return {}
        alma_base_url = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/{}?password={}" \
                        "&apikey={}".format(user_bib_num.lower(), pwd, current_app.config["ALMA_API_KEY"])
        #alma_base_url2 = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?item_barcode={}".format("23189057960005504")
        #  'user_group': {'desc': ' Academic Staff', 'value': '05'},
        sandbox = 0
        params = {"apikey": current_app.config["ALMA_API_SANDBOX_KEY"]} if sandbox else {"apikey": current_app.config["ALMA_API_KEY"]}
        params.update({"password": pwd, "op": "auth"})
        headers = {"accept": "application/json"}
        user_request = requests.post(alma_base_url, headers=headers)
        user_data = UBUnibasAlmaLogin.get_alma_user(user_bib_num)

        if user_request.ok and user_data:
            if user_data:
                conn = sqlite3.connect(current_app.config["AUTH_DB"])
                c = conn.cursor()
                user_data_dump = json.dumps(user_data)
                c.execute("INSERT OR REPLACE INTO auth_users VALUES (?, ?)", [user_bib_num, user_data_dump])
                conn.commit()
                conn.close()
            return user_data
        else:
            return {}

    @staticmethod
    def get_alma_user(user_bib_num):
        user = {}
        # user_bib_num = "martin.reisacher@unibas.ch"
        # user_bib_num = "mbs01130962"
        if not user_bib_num:
            return {}
        alma_base_url = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/{}".format(user_bib_num.lower())
        #alma_base_url2 = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?item_barcode={}".format("23189057960005504")
        #  'user_group': {'desc': ' Academic Staff', 'value': '05'},
        sandbox = current_app.config.get("ALMA_SANDBOX")
        params = {"apikey": current_app.config["ALMA_API_SANDBOX_KEY"]} if sandbox else {"apikey": current_app.config["ALMA_API_KEY"]}
        headers = {"accept": "application/json"}
        resp = requests.get(alma_base_url, params=params, headers=headers).json()
        if resp.get("errorsExist"):
            return {}
        else:
            return UBUnibasAlmaLogin.extract_alma_user(resp)

    @staticmethod
    def extract_alma_user(resp):
        # import pprint
        # pprint.pprint(resp)

        primary_id = resp.get("primary_id", {})
        full_name = resp.get("full_name", {})
        user_group = resp.get("user_group", {})
        phone = ""
        preferred_email = ""
        emails = []
        for slsp_phone in resp.get("contact_info", {}).get("phone", {}):
            if slsp_phone.get("preferred", {}):
                phone = slsp_phone.get("phone_number", "")
        for slsp_mail in resp.get("contact_info", {}).get("email", {}):
            emails.append(slsp_mail.get("email_address", "").lower())
            if slsp_mail.get("preferred", {}):
                preferred_email = slsp_mail.get("email_address", "").lower()
        adress_data = {}

        for slsp_adress in resp.get("contact_info", {}).get("address", {}):
            adress_type = slsp_adress.get("address_note", {})
            plz = "{}-{} {}".format(slsp_adress.get("country", {}).get("value"), slsp_adress.get("postal_code"),
                                    slsp_adress.get("city"))
            adress = ", ".join(
                [slsp_adress.get("line{}".format(n)) for n in [1, 2, 3, 4] if slsp_adress.get("line{}".format(n))])
            adress_entry = {"slsp_adresse": adress, "slsp_plz": plz}
            adress_data[adress_type] = adress_entry
            if slsp_adress.get("preferred", {}):
                adress_data["preferred"] = adress_entry

        user = {"slsp_phone": phone, "slsp_mail": preferred_email, "slsp_emails_hidden": emails,
                "slsp_id_hidden": primary_id,
                "slsp_institution": ", ".join(adress_data.get("Verified by organisation", {}).values()),
                "slsp_full_name": full_name, "slsp_usergroup_hidden": user_group.get("value"),
                "slsp_usergroup_desc_hidden": user_group.get("desc")}
        # bestellsystem-gruppe-beschreibung()
        user.update(adress_data.get("preferred", {}))

        return user



class UBUnibasAlephLogin(UserMixin):

    def __init__(self, uid, pwd, host="aleph.unibas.ch", library="dsv51"):
        self.uid = uid
        self.pwd = pwd
        self.url = "http://{}/X?op=bor-auth&library={}&bor_id={}" \
                   "&verification={}".format(host, library, uid, pwd)
        self.xml_login_response =  self.query_aleph() #ElementTree.fromstring(open("/home/martin/PycharmProjects/auth/test.xml").read()) #

    def query_aleph(self):
        try:
            xml_data = requests.get(self.url).text
            xml_login_response = ElementTree.fromstring(xml_data)
        except Exception:
            import traceback
            traceback.print_exc()
            xml_login_response = ""
        return xml_login_response

    @property
    def is_anonymous(self):
        if not self.uid:
            return True

    @property
    def is_authenticated(self):
        if self.xml_login_response and self.is_active:

            # print(pickeled_auth_user)
            if not self.get_id():
                conn = sqlite3.connect(current_app.config["AUTH_DB"])
                c = conn.cursor()
                pickeled_auth_user = pickle.dumps(self)
                c.execute("INSERT INTO auth_users VALUES (?, ?)", [self.uid, pickeled_auth_user])
                conn.commit()
                conn.close()
            return True
        else:
            return False

    @property
    def user_data(self):
        if self.xml_login_response:
            print(self.xml_login_response, self.xml_login_response.findall("z303"))
            for z303 in self.xml_login_response.findall("z303") or []:
                try:
                    slsp_full_name = z303.find("z303-name").text
                except Exception:
                    slsp_full_name = "nicht angegeben"
                try:
                    slsp_birthdate = z303.find("z303-birth-date").text
                except Exception:
                    slsp_birthdate = "nicht angegeben"
            for z304 in self.xml_login_response.findall("z304") or []:
                try:
                    slsp_mail = z304.find("z304-email-address").text
                except Exception:
                    slsp_mail = "nicht angegeben"
            return {"slsp_full_name": slsp_full_name, "slsp_birthdate": slsp_birthdate, "slsp_mail": slsp_mail}
        else:
            return False

    @property
    def is_active(self):
        if self.xml_login_response:
            for z305 in self.xml_login_response.findall("z305") or []:
                expiry = z305.find("z305-expiry-date").text
                sub_lib = z305.find("z305-sub-library").text
                expiry_date = datetime.datetime.strptime(expiry, "%d/%m/%Y")
                # delinquency note?
                if expiry_date > datetime.datetime.now():
                    self.id = self.uid
                    return True
                else:
                    return False
        else:
            return False

