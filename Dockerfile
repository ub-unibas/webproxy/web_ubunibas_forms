FROM python:3.8

ENV FLASK_APP unibas_forms
ENV MODE prod

RUN useradd -m -s /bin/bash appuser
RUN apt update && apt install sqlite3 -y

WORKDIR /
RUN mkdir data
RUN mkdir configs
RUN mkdir app
ADD setup.py /app
ADD requirements.txt /app

WORKDIR /app
RUN python -m pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt

WORKDIR /
ADD uwsgi_webforms.py /app
ADD uwsgi_webforms.ini /app
ADD unibas_forms app/unibas_forms
ADD oidc_auth app/oidc_auth
ADD k8s-manifests/config/gunicorn.conf.py /configs/

WORKDIR /app
EXPOSE 5000

ENTRYPOINT ["gunicorn"]
CMD ["uwsgi_webforms:unibas_form_app", "--config", "/configs/gunicorn.conf.py"]
