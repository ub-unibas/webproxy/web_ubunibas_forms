from unibas_forms import unibas_form_app
import logging
from flask.logging import default_handler

if __name__ == "__main__":
    unibas_form_app.run()

# If app is started via gunicorn
if __name__ != '__main__':
    gunicorn_logger = logging.getLogger('gunicorn.logger')
    unibas_form_app.logger.handlers = gunicorn_logger.handlers
    unibas_form_app.logger.setLevel(gunicorn_logger.level)
    unibas_form_app.logger.removeHandler(default_handler)
    unibas_form_app.logger.info('Starting production server')
