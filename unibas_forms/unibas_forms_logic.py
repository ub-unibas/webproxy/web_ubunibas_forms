import re
import os
import json
import requests
import smtplib
import datetime
from copy import deepcopy
from collections import OrderedDict
from string import Template
from email.message import EmailMessage
from email import charset
from flask import redirect, render_template, current_app
from flask_login import LoginManager, login_required, current_user, logout_user, login_user, UserMixin
from flask_wtf import FlaskForm

from wtforms import BooleanField, StringField, PasswordField, validators, \
    SubmitField, SelectField, RadioField, TextAreaField, ValidationError, HiddenField
from wtforms.validators import Length


# do not send mails, mails to stdout
TEST_MAIL_TO = "martin.reisacher@unibas.ch"
TEST_MAIL_MYBIBTO = "martin.reisacher@unibas.ch" # "mybib-basel@ub.unibe.ch" #"martin.reisacher@unibas.ch" #mylibtest@stub.unibe.ch

MAIL_MYBIBTO =  "form2mybib-ub@unibas.ch" #"mybib-basel@ub.unibe.ch" #
MAIL_MYBIBTO_TEST = "form2mybib-ub@unibas.ch" # form2mybib-ub@unibas.ch martin.reisacher@unibas.ch


# TODOS:
    # counter neu programmieren
    # besseres Fehlerhandling beim Mailversand
    # mailkategorien fett drucken

"""SFXILL        | 136607 | admin | 2020-12-02 15:44:46 |
|  2 | TEST          |     82 | admin | 2020-11-30 14:07:59 |
|  5 | SFX_DOD_UB    |   4302 | admin | 2013-04-02 07:18:11 |
|  6 | SFX_DOD_WWZ   |    445 | admin | 2016-03-29 09:16:37 |
|  7 | SFX_DOD_TEST  |    761 | admin | 2020-04-12 14:17:28 |
|  9 | WEBILL"""

# Andres todos:
    # adress Korrekturen
    # issn / isbn Regeln
    # open url korrekturen

# Fragen Bern
    # ausloggen sinnvoll?


class Auth_Form:
    form_title = "Auth Form"
    login_service = "login"
    specific_parts = {}
    template = "auth_form_temp.html"
    smtp_host = "smtp.unibas.ch"
    mail_from = "UB Info <info-ub@unibas.ch>"
    mail_to = "info-ub@unibas.ch"
    subject = ""

    submit = SubmitField('Weiter')
    confirm = SubmitField('Bestätigen')
    back = SubmitField('Zurück')
    reload = SubmitField('Reload')
    new = SubmitField('Neues Formular')
    logout = SubmitField('Zurück zur Anmeldung')

    slsp_full_name = HiddenField("Name")
    slsp_institution = HiddenField("Institution")
    slsp_adresse = HiddenField("Adresse")
    slsp_plz = HiddenField("Stadt")
    slsp_mail = HiddenField("Email")
    slsp_phone = HiddenField("Telefon")

    slsp_id_hidden = HiddenField("eduID")
    slsp_usergroup_hidden = HiddenField("userGroupID")
    slsp_usergroup_desc_hidden = HiddenField("userGroupValue")

    signatur = """UNIVERSITÄT BASEL | Universitätsbibliothek | Information und Fernleihe
    Schönbeinstrasse 18-20 | 4056 Basel | Schweiz        
    Tel +41 61 207 31 00 | Fax +41 61 207 31 03
    E-Mail info-ub@unibas.ch | www.ub.unibas.ch"""

    @classmethod
    def form_auth_template(cls, request, user, remove_empty_fields=False, header=False, mode="debug", backup_dir=None):
        try:
            self = cls(request.form)
            return self.build_form_template(request=request, user=user, remove_empty_fields=remove_empty_fields, header=header, mode=mode, backup_dir=backup_dir)
        except Exception as e:
            import traceback
            traceback.print_exc()
            return render_template(template_name_or_list="message_template.html", message=str(e.args[0]),
                                   header=False, error=True)


    def build_form_template(self, request, user, remove_empty_fields=False, header=False, mode="debug", backup_dir=None):

        self.backup_dir = backup_dir
        form_title = "{} {}".format(mode.upper(), self.form_title) if mode != "prod" else self.form_title
        print("USER", user)
        alma_user = self.get_alma_user(user)
        eduid_service = None
        # wird zum Versenden der Mails gebraucht
        self.mailuser = (alma_user.get("slsp_mail") or "")
        # to extract multiple get params with same key
        request_args = OrderedDict()
        for entry in request.args.lists():
            entries = []
            for e in entry[1]:
                if e not in entries and e:
                    entries.append(e)
            request_args[entry[0]] = "; ".join([e for e in entries])

        update_args = deepcopy(request_args)
        update_args.update(alma_user)

        if self.logout.data:
            logout_user()
            return redirect(request.script_root + request.path)
        if request.method == 'POST' and self.validate() and (self.confirm.data or self.submit.data):

            if self.confirm.data:
                # for open_url values and user data
                self.send_formdata_mail(request, mode, alma_user)
                self.process(formdata=request.form, obj={}, **alma_user)
                return render_template(self.template, title=form_title,
                                       form=self, eduid_service=eduid_service, user=alma_user,
                                       remove_empty_fields=remove_empty_fields, status="finished", header=header,
                                       specific_parts=self.specific_parts)
            # confirm entries
            elif self.submit.data:
                # for open_url values and user data
                self.process(formdata=request.form, obj={}, **alma_user)
                return render_template(self.template, title=form_title,
                                       form=self, eduid_service=eduid_service, user=alma_user,
                                       remove_empty_fields=remove_empty_fields, status="confirm", header=header,
                                       specific_parts=self.specific_parts)
        elif self.new.data:
            return redirect(request.script_root + request.path)
        else:
            # for open_url values
            self.process(formdata=request.form, obj={}, **request_args)
            return render_template(self.template, title=form_title,
                                   form=self, eduid_service=eduid_service, user=alma_user,
                                   remove_empty_fields=remove_empty_fields, status="fillout", header=header,
                                   specific_parts=self.specific_parts)

    def get_form_lookups(self):

        field_names = {field.id: field.label.text for field in
                       self if field.type not in ["SubmitField", "CSRFTokenField"] and field.id != 'man_benutzernummer' }
        value_lookup = {}
        for field in self:
            if field.type in ["SelectField", "RadioField"]:
                key = field.label.field_id
                value_lookup.setdefault(key, {})
                for subkey in field.choices:
                    value_lookup[key][subkey[0]] = subkey[1]
        return field_names, value_lookup

    def build_mailcontent(self, add_content, request):
        field_names, value_lookup = self.get_form_lookups()
        mailcontent = ""
        form_values = {}
        for key, val in add_content.items():
            # {:.<40}
            if key in field_names or key in ["Bestelldatum", "Auftragsnummer"]:
                if not key.endswith("_hidden"):
                    form_values[key] = val
                    if value_lookup.get(key):
                        val = value_lookup.get(key).get(val)
                    if val:
                        mailcontent += "{}: {}\n".format(field_names.get(key, key), val)
            form_values[key] = val
        mailcontent += "\n"
        for key, val in request.form.items():
            if key in field_names:
                if not key.endswith("_hidden"):
                    form_values[key] = val
                    if value_lookup.get(key):
                        val = value_lookup.get(key).get(val)
                    mailcontent += "{}: {}\n".format(field_names.get(key, key), val)
        return mailcontent, form_values

    def send_formdata_mail(self, request, mode="debug", update_args={}):
        add_content = update_args
        add_content["Bestelldatum"] = datetime.datetime.now().strftime("%d.%m.%Y, %H:%M:%S")
        mailcontent, form_values = self.build_mailcontent(add_content, request)
        form_values.update(add_content)
        self.send_mail(mailcontent, form_values, mode=mode)

    def send_mail(self, mailcontent, form_values, mode="debug", mail_from=None, mail_to=None):
        """
        $mailtext = $u2a->convert($mailtext);
        $Mail::Sender::NO_X_MAILER = 1;
                charset =>  'ISO-8859-1',
            encoding => '8BIT',
        """

        patron_msg = self.get_patron_msg(form_values, mailcontent, mode=mode)
        ticket_msg = self.get_ticket_msg(form_values, mailcontent, mode=mode)

        # Test
        if mode=="debug":
            print(patron_msg)
            print(ticket_msg)
            print(mailcontent)
        else:
            try:
                s = smtplib.SMTP(self.smtp_host)
                s.send_message(patron_msg)
                s.send_message(ticket_msg)
                s.quit()
            except Exception:
                raise Exception("Problem mit dem Mailversand, wenden Sie sich bitte an it-ub@unibas.ch")

    def get_patron_msg(self, form_values, mailcontent, mode="debug", mail_from=None, mail_to=None):
        msg = EmailMessage()
        mailcontent = "Wir danken für Ihren Auftrag. Dieser " \
                      "wird schnellstmöglichst bearbeitet. \n\n" \
                      "{}\n\nMit freundlichen Grüssen\n\n{}".format(mailcontent, self.signatur)
        msg.set_content(mailcontent)
        msg['Subject'] = self.subject or "{}Bestätigung {} UB Basel".format(mode.upper() + " " if mode != "prod" else "", self.form_title )
        msg['From'] = mail_from or self.get_mail_from_patron(form_values, mode=mode)
        # todo: anpassen
        msg['To'] = self.get_mail_to_patron(form_values, mode=mode)
        return msg

    def get_ticket_msg(self, form_values, mailcontent, mode="debug", mail_from=None, mail_to=None):
        msg = EmailMessage()
        mailcontent = "Wir danken für Ihren Auftrag. Dieser " \
                      "wird schnellstmöglichst bearbeitet. \n\n" \
                      "{}\n\nMit freundlichen Grüssen\n\n{}".format(mailcontent, self.signatur)
        msg.set_content(mailcontent)
        msg['Subject'] = self.subject or "{}Ticket: {}".format(mode.upper() + " " if mode != "prod" else "" , self.form_title)
        msg['From'] = mail_from or self.get_mail_from(form_values, mode=mode)
        msg['To'] = mail_to or self.get_mail_to(form_values, mode=mode)
        return msg

    def get_mail_from(self, form_values, mode="debug"):
        return self.mail_from

    def get_mail_from_patron(self, form_values, mode="debug"):
        return self.mail_from

    def get_mail_to(self, form_values, mode="debug"):
        return self.mail_to if mode=="prod" else self.mailuser #TEST_MAIL_TO

    def get_mail_to_patron(self, form_values, mode="debug"):
        return self.mail_to if mode=="prod" else self.mailuser #TEST_MAIL_TO

    @staticmethod
    def get_alma_user(user_bib_num):
        user = {}
        # user_bib_num = "martin.reisacher@unibas.ch"
        # user_bib_num = "mbs01130962"
        if not user_bib_num:
            return {}
        alma_base_url = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/{}".format(user_bib_num.lower())
        #alma_base_url2 = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?item_barcode={}".format("23189057960005504")
        #  'user_group': {'desc': ' Academic Staff', 'value': '05'},
        sandbox = current_app.config.get("ALMA_SANDBOX")
        params = {"apikey": current_app.config["ALMA_API_SANDBOX_KEY"]} if sandbox else {"apikey": current_app.config["ALMA_API_KEY"]}
        headers = {"accept": "application/json"}
        resp = requests.get(alma_base_url, params=params, headers=headers).json()
        print(resp)
        return Auth_Form.extract_alma_user(resp)

    @staticmethod
    def get_alma_firmen_user(user_bib_num, pwd):
        user = {}
        # user_bib_num = "martin.reisacher@unibas.ch"
        # user_bib_num = "mbs01130962"
        if not user_bib_num:
            return {}
        alma_base_url = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/users/{}?password={}" \
                        "&apikey={}".format(user_bib_num.lower(), pwd, current_app.config["ALMA_API_KEY"])
        #alma_base_url2 = "https://api-eu.hosted.exlibrisgroup.com/almaws/v1/items?item_barcode={}".format("23189057960005504")
        #  'user_group': {'desc': ' Academic Staff', 'value': '05'},
        sandbox = 0
        params = {"apikey": current_app.config["ALMA_API_SANDBOX_KEY"]} if sandbox else {"apikey": current_app.config["ALMA_API_KEY"]}
        params.update({"password": pwd, "op": "auth"})
        headers = {"accept": "application/json"}
        user_request = requests.post(alma_base_url, headers=headers)
        if user_request.ok:
            return Auth_Form.get_alma_user(user_bib_num)
        else:
            return {}

    @staticmethod
    def extract_alma_user(resp):
        #import pprint
        #pprint.pprint(resp)

        primary_id =  resp.get("primary_id",{})
        full_name = resp.get("full_name",{})
        user_group = resp.get("user_group", {})
        phone = ""
        preferred_email = ""
        emails = []
        for slsp_phone in resp.get("contact_info",{}).get("phone", {}):
            if slsp_phone.get("preferred",{}):
                phone = slsp_phone.get("phone_number","")
        for slsp_mail in resp.get("contact_info",{}).get("email", {}):
            emails.append(slsp_mail.get("email_address","").lower())
            if slsp_mail.get("preferred", {}):
                preferred_email = slsp_mail.get("email_address","").lower()
        adress_data = {}

        for slsp_adress in resp.get("contact_info",{}).get("address", {}):
            adress_type = slsp_adress.get("address_note",{})
            plz = "{}-{} {}".format(slsp_adress.get("country",{}).get("value"), slsp_adress.get("postal_code"), slsp_adress.get("city"))
            adress = ", ".join([slsp_adress.get("line{}".format(n)) for n in [1,2,3,4] if slsp_adress.get("line{}".format(n))])
            adress_entry = {"slsp_adresse": adress, "slsp_plz": plz}
            adress_data[adress_type] = adress_entry
            if slsp_adress.get("preferred", {}):
                adress_data["preferred"] = adress_entry

        user = {"slsp_phone": phone, "slsp_mail": preferred_email, "slsp_emails_hidden": emails, "slsp_id_hidden": primary_id,
                "slsp_institution": ", ".join(adress_data.get("Verified by organisation", {}).values()),
                "slsp_full_name": full_name, "slsp_usergroup_hidden": user_group.get("value"),
                "slsp_usergroup_desc_hidden": user_group.get("desc")}
        #bestellsystem-gruppe-beschreibung()
        user.update(adress_data.get("preferred",{}))

        return user

#print(Auth_Form.get_alma_user("MBS01101276"))


class MyBibTemplate:

    def get_autragsnummer(self, mode="debug"):
        from unibas_forms.unibas_counter import CounterMyBib
        from unibas_forms import auth_db
        try:
            prefix_choice = self.settings.get("Counter")
            nc = CounterMyBib.query.filter_by(prefix=prefix_choice).first()

            nc.date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
            if mode != "debug":
                nc.nc = nc.nc + 1
                auth_db.session.commit()

            # Todo: SFX und Paper ersetzen
            mybib_auftragsnummer = "{} {} {}".format(self.settings.get("MybibDomain"), self.nc_prefix, nc.nc)
            return mybib_auftragsnummer
        except Exception:
            import traceback
            traceback.print_exc()
            raise  Exception("Problem mit der Rechnungsnummervergabe, bitte wenden Sie sich an it-ub@unibas.ch")

    def get_autragsnummer_old(self, mode):
        # problem mit Firewall - nicht im Einsatz
        try:
            counter_resp = requests.get("{}{}".format(self.counter_url, self.settings.get("Counter")))
            if counter_resp.ok:
                # form ok|552
                counter = counter_resp.text.split("|")[-1]
            else:
                counter = "TEST" if mode != "prod" else "ID-Counter not available"
            return "{} {} {}".format(self.settings.get("MybibDomain"), "SFX", counter)
        except requests.ConnectionError:
            return "Problem mit der Rechnungsnummervergabe, bitte wenden Sie sich an it-ub@unibas.ch"

    def send_formdata_mail(self, request, mode="debug", update_args = {}):
        add_content = update_args
        add_content["Bestelldatum"] = datetime.datetime.now().strftime("%d.%m.%Y %H:%M:%S")
        add_content["Auftragsnummer"] = self.get_autragsnummer(mode)

        mailcontent, form_values = self.build_mailcontent(add_content, request)
        #nachdem der mail body gebaut wurde
        add_content["open_url"] = request.url
        add_content["merged_Kommentar"] = " | ".join(["{}: {}".format(key, form_values.get(key))
                                                      for key in ["meduid", "source", "man_kommentar"] if
                                                      form_values.get(key)])
        form_values.update(add_content)
        self.send_mail(mailcontent, form_values, mode=mode)

    def send_mail(self, mailcontent, form_values, mode="debug", mail_from=None, mail_to=None):
        """
        $mailtext = $u2a->convert($mailtext);
        $Mail::Sender::NO_X_MAILER = 1;
                charset =>  'ISO-8859-1',
            encoding => '8BIT',
        """
        mybib_msg = self.get_ticket_msg(form_values, mailcontent, mode=mode)
        patron_msg = self.get_patron_msg(form_values, mailcontent, mode=mode)
        print(mybib_msg)
        if mode == "debug":
            print(patron_msg)
            print(mybib_msg)
            print(mailcontent)
            print(self.build_mybib_format(form_values))
        else:
            s = smtplib.SMTP(self.smtp_host)
            s.send_message(mybib_msg)
            s.send_message(patron_msg)
            s.quit()

    def get_patron_msg(self, form_values, mailcontent, mode="debug", mail_from=None, mail_to=None):
        msg = EmailMessage()
        mailcontent = "Wir danken für Ihren Auftrag. Ihre Kopienbestellung " \
                      "wird schnellstmöglichst bearbeitet. \n\n" \
                      "{}\n\nMit freundlichen Grüssen\n\n{}".format(mailcontent, self.signatur)
        msg.set_content(mailcontent)
        msg['Subject'] = "{}Dokumentbestellung UB Basel".format(mode.upper() + " " if mode != "prod" else "" )
        msg['From'] = mail_from or self.get_mail_from(form_values)
        msg['To'] = self.mailuser if mode == "prod" else self.mailuser #TEST_MAIL_TO
        return msg

    def get_ticket_msg(self, form_values, mailcontent, mode="debug", mail_from=None, mail_to=None):
        """
        $mailtext = $u2a->convert($mailtext);
        $Mail::Sender::NO_X_MAILER = 1;
                charset =>  'ISO-8859-1',
            encoding => '8BIT',
        """
        msg = EmailMessage()
        #print(self.build_mybib_format(form_values))
        msg.set_content(self.build_mybib_format(form_values))
        #msg.set_content(self.build_mybib_format(form_values).encode("utf-8").decode("iso-8859-1"))
        #ch = charset.Charset('iso-8859-1')
        #ch.body_encoding = '8bit'
        #msg.set_param('charset', "iso-8859-1")
        #msg.replace_header('Content-Transfer-Encoding', '8bit')
        msg['Subject'] = "{}DOD Auftrag swisscovery Basel".format(mode.upper() + " " if mode != "prod" else "" )
        msg['From'] = mail_from or self.get_mail_from(form_values)
        msg['To'] = self.settings.get("MybibEmail") if mode == "prod" else self.settings.get("MybibEmail")
        return msg

# ganze wichtig, dass MyBibTemplate vor AuthForm eingebunden wird
class MyBibTemplatePrimoTemplate(MyBibTemplate, Auth_Form):


    counter_url = "https://intraweb.ub.unibas.ch/php/admin/counter.php?next="

    # Values werden für Kopien LinkResolver Formularautomatisch aus der OpenUrl ausgelesen -> Variable-Name fungiert als Key
    title = StringField('Titel des Artikels/Beitrags', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    jbtitle = StringField('Zeitschriften- / Buchtitel', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    author = StringField('AutorIn')
    year = StringField('Jahr')
    volume = StringField('Volume')
    issue = StringField('Issue')
    pages = StringField('Seiten')
    issn = StringField('ISSN')

    def build_mybib_format(self, form_values):
        # kommentarlabels nicht ganz korrekt, eigentlich Bemerkung, Source, PMID/AN
        # kein check, ob ISSN mit iSBN: anfängt
        backup_path = "{}.json".format(os.path.join(self.backup_dir,form_values.get("Auftragsnummer"))).replace(" ", "-")
        escaped_form_values ={k: v.replace(")", "\)") if isinstance(v, str) else v for k,v in form_values.items()}
        json.dump(form_values, open(backup_path, "w"))
        mybib = """typ(FES)
subtyp(FES_ZEITSCHRIFT)
version(1.2)
auftragsnummer($Auftragsnummer)
bestelldatum($Bestelldatum)
lieferart($man_lieferart)
lieferformat(PDF)
liefertyp(COPY)
lieferzusatz($man_ausland)
artikel-gesamt-titel($jbtitle)
artikel-titel($title)
artikel-autor($author)
artikel-erscheinungsjahr($year)
artikel-erscheinungsort()
artikel-seitenangabe($pages)
artikel-issn($issn)
artikel-isbn($isbn)
artikel-ausgabe(Volume: $volume - Issue: $issue)
artikel-band($volume)
artikel-heft($issue)
artikel-referenz-quelle($docurl)
mediennummer($meduid)
auftraggeber-name($slsp_full_name)
besteller-name($slsp_full_name)
besteller-kundennummer($slsp_id_hidden)
besteller-institution($slsp_institution)
besteller-strasse($slsp_adresse)
besteller-stadt($slsp_plz)
kontakt-telefon($slsp_phone)
besteller-email($slsp_mail)
bestellsystem-kommentar($merged_Kommentar)
bestellsystem-gruppe($slsp_usergroup_hidden)
bestellsystem-gruppe-beschreibung($slsp_usergroup_desc_hidden)"""

        mybib_result = Template(mybib).safe_substitute(**escaped_form_values)
        mybib_result = re.sub("\$[^ \(\)]*", "", mybib_result)
        return mybib_result



# import MyBibTemplate before AuthForm otherwise methods are not overwritten
class Kopienbestellung_LinkResolver_Base(FlaskForm, MyBibTemplatePrimoTemplate):
    form_title = "Kopienbestellung LinkResolver"
    nc_prefix = "SFX"
    specific_parts = {"top": "proj/koplink/iframe_top.html"}
    settings = {"MybibDomain": "BS Test", "MybibEmail": "martin.reisacher@unibas.ch",
                "EdocMail": "martin.reisacher@unibas.ch", "LibraryName": "UB Basel (TEST)",
                "Counter": "SFX_DOD_TEST", "LibCode": "A100"}
    signatur = """UNIVERSITÄT BASEL | Universitätsbibliothek | Information und Fernleihe
Schönbeinstrasse 18-20 | 4056 Basel | Schweiz        
Tel +41 61 207 31 00 | Fax +41 61 207 31 03
E-Mail info-ub@unibas.ch | www.ub.unibas.ch"""

    mail_from = "UB Info <info-ub@unibas.ch>"

    # Values werden automatisch aus der OpenUrl ausgelesen -> Variable-Name fungiert als Key
    isbn = StringField('ISBN')
    meduid = StringField('Meduid')
    source = StringField('Quelle')
    docurl = StringField('Document Uri')

    man_kommentar = TextAreaField('Bemerkungen, weitere Angaben', render_kw={'class': 'text-field'}, validators=[Length(max=255, message="Feld darf maximal 255 Zeichen haben.")])
    man_lieferart = RadioField('Versandart', choices=[('WEB', 'Elektronisch (wenn möglich)*'),
                                                ('POST', 'Postversand')], default="WEB")


class Kopienbestellung_LinkResolver_Test(Kopienbestellung_LinkResolver_Base):
    form_title = "Kopienbestellung LinkResolver (MyBib/Counter Testsettings)"
    settings = {"MybibDomain": "BS Test", "MybibEmail": MAIL_MYBIBTO_TEST,
                "EdocMail": "martin.reisacher@unibas.ch", "LibraryName": "UB Basel (TEST)",
                "Counter": "SFX_DOD_TEST", "LibCode": "A100"}

    def get_mail_to(self, form_values, mode = "debug"):
        return "MyBIB <{}>".format(self.mailuser) #TEST_MAIL_TO)

class Kopienbestellung_LinkResolver_UB(Kopienbestellung_LinkResolver_Base):
    form_title = "Kopienbestellung LinkResolver UB"
    settings = {"MybibDomain": "BS UB", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "info-ub@unibas.ch", "LibraryName": "Universitätsbibliothek Basel",
                "Counter": "SFX_DOD_UB", "LibCode": "A100"}


class Kopienbestellung_LinkResolver_UBM(Kopienbestellung_LinkResolver_Base):
    form_title = "Kopienbestellung LinkResolver UBM"
    settings = {"MybibDomain": "BS Medbib", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "info-medb@unibas.ch", "LibraryName": "UB Medizin, Basel",
                "Counter": "SFXILL", "LibCode": "A140"}
    signatur = """UNIVERSITÄT BASEL
UB Medizin
Information und Fernleihe
info-medb@unibas.ch
Tel +41 61 207 32 00"""

class Kopienbestellung_LinkResolver_UBW(Kopienbestellung_LinkResolver_Base):
    form_title = "Kopienbestellung LinkResolver UBW"
    settings = {"MybibDomain": "BS WWZ", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "kopien-ubw-swa@unibas.ch", "LibraryName": "UB Wirtschaft - SWA, Basel",
                "Counter": "SFX_DOD_WWZ", "LibCode": "A125"}
    signatur = """UNIVERSITÄT BASEL
UB Wirtschaft - SWA
Information und Fernleihe
ill-ubw-swa@unibas.ch"""

# import MyBibTemplate before AuthForm otherwise methods are not overwritten
class Kopienbestellung(FlaskForm, MyBibTemplatePrimoTemplate):
    """
        my $AUFTRAGSNUMMER = $Config->{MybibDomain} .' WEB ' .$nc;
    $Config->{AUFTRAGSNUMMER} = $AUFTRAGSNUMMER;
    my $mailmsg = format_mybib();
    my $subject = 'WWW Kopienbestellung ' . $AUFTRAGSNUMMER;
    my $from    = 'WWW Kopienbestellung <' .$Config->{PaperEmail} .'>';
    $Config->{AUFTRAGSNUMMER} = 'Mybib ' .$AUFTRAGSNUMMER;

    """
    form_title = "Kopienbestellung"
    nc_prefix = "WEB"
    specific_parts = {"top": "proj/koplink/iframe_top.html"}

    settings = {"MybibDomain": "BS UB", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "info-ub@unibas.ch", "LibraryName": "Universitätsbibliothek Basel",
                "Counter": "WEBILL", "LibCode": "A100"}

    pages = StringField('Seiten', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    man_kommentar = TextAreaField('Bemerkung, weitere Angaben', validators=[Length(max=255, message="Feld darf maximal 255 Zeichen haben.")])
    man_lieferart = SelectField('Versandart', choices=[('WEB', 'Elektronisch (wenn möglich)*'),
                                                    ('POST', 'Postversand')], default="WEB")
    man_ausland = SelectField('kann auch im Ausland bestellt werden', choices=[('Nur in der Schweiz bestellen', 'Nein'),
                                                                           ('Kann auch im Ausland bestellt werden', 'Ja')], default="Nur in der Schweiz bestellen")

class KopienbestellungTest(Kopienbestellung):
    form_title = "Kopienbestellung Test"
    specific_parts = {"top": "proj/koplink/iframe_top.html"}
    settings = {"MybibDomain": "BS Test", "MybibEmail": MAIL_MYBIBTO_TEST,
                "EdocMail": "martin.reisacher@unibas.ch", "LibraryName": "UB Basel (TEST)",
                "Counter": "SFX_DOD_TEST", "LibCode": "A100"}

    def get_mail_to(self, form_values, mode = "debug"):
        return "MyBIB <{}>".format(self.mailuser) #TEST_MAIL_TO)

# import MyBibTemplate before AuthForm otherwise methods are not overwritten




class Anschaffungsvorschlag(FlaskForm, Auth_Form):
    form_title = "Anschaffungsvorschlag"
    subject = ""
    # defined in blueprint, so cache is updated
    #fachgebiete = {}
    autor = StringField('AutorIn')
    titel = StringField('Titel', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    verlagsort = StringField('Verlagsort, Verlag')
    erscheinungsjahr = StringField('Erscheinungsjahr')
    isbn = StringField('ISBN')
    kommentar = TextAreaField('Kommentar')
    quelle = StringField('Quelle Ihrer Angaben')
    # defined in blueprint, so cache is updated
    fachgebiet = SelectField('Fachgebiet', [validators.DataRequired(message="Feld muss ausgefüllt sein")],
                                 choices=[(""," -- Bitte auswählen -- ")])
    version = RadioField('Benachrichtigen Sie mich...', choices=[('value_one', 'Ich wünsche keine Benachrichtigung.'),
                                                                 ('value_two',
                                                                  'Bitte teilen Sie mir per Email mit, ob das Buch angeschafft wird.'),
                                                                 ('value_three',
                                                                  'Bitte teilen Sie mir per Email mit, ob das Buch angeschafft wird '
                                                                  'und benachrichtigen Sie mich, wenn es für mich bereitsteht.')],
                         default="value_one")
    # abholort auf Wunsch von Dorothea deaktiviert
    # abholort = SelectField('Abholort', choices=[('ub_hauptbibliothek', 'UB Hauptbibliothek'),('ub_wirtschaft', 'UB Wirtschaft - SWA'),('ub_medizin', 'UB Medizin')])
    ebook = RadioField('Soll es, falls lieferbar, als E-Book bestellt werden?',
                       choices=[('ja', 'Ja'), ('nein', 'Nein')], default="nein")


    def build_form_template(self, request, user, remove_empty_fields=False, header=False, mode="debug", backup_dir=None):
        fachgebiete = self.get_fachgebiete()
        self.fachgebiet.choices=[(""," -- Bitte auswählen -- ")] + [("{} <{}>".format(k.replace(",",""), v), k) for k, v in fachgebiete.items()]
        return super().build_form_template(request=request, user=user, remove_empty_fields=remove_empty_fields, header=header, mode=mode, backup_dir=backup_dir)

    def get_fachgebiete(self):
        try:
            # else out of context
            from gdspreadsheets_ubit import PygExtendedClient, authorize_pyg
            from cache_decorator_redis_ubit import SettingsShortCacheDecorator, SettingsCacheDecorator12h
            from wtforms.widgets import PasswordInput
            from unibas_forms import MODE
            service_file = current_app.config.get("GD_SERVICE_FILE")
            spreadsheet_id = "1FYIev4QsW_3q_VOtuAzJG4sUCYq0R9QI0khAxXzKlmQ"
            sheet = "Anschaffungsvorschlag"
            key_field = "Fach"
            cache_decorator = SettingsCacheDecorator12h if MODE == "prod" else SettingsShortCacheDecorator
            PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var='GOOGLE_DOC_API_KEY',
                                                 service_file=service_file,
                                                 cache_decorator=cache_decorator)
            spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
            max_sheet = spreadsheet.worksheet_by_title(sheet)
            fachgebiete = {k: v["Mailadresse"] for k, v in max_sheet.gSpreadsheetKeyDict(key_field).items()}
        except Exception as e:
            import traceback
            traceback.print_exc()
            fachgebiete = {
                "Allgemein": "info-ub@unibas.ch",
                "Ägyptologie, Assyriologie": "christoph.schneider@unibas.ch",
                "Anglistik": "johanna.schuepbach@unibas.ch",
                "Archäologie, Urgeschichte": "christoph.schneider@unibas.ch",
                "Architektur": "noah.regenass@unibas.ch",
                "Belletristik (deutsch)": "susanne.gubser@unibas.ch",
                "Belletristik (englisch)": "johanna.schuepbach@unibas.ch",
                "Belletristik (französisch)": "yvonne.hauser@unibas.ch",
                "Belletristik (weitere Sprachen)": "info-ub@unibas.ch",
                "Biologie": "simon.geiger@unibas.ch",
                "Buch- und Bibliothekswesen": "noah.regenass@unibas.ch",
                "Chemie": "simon.geiger@unibas.ch",
                "Ethnologie, Volkskunde": "Alice.Spinnler@unibas.ch",
                "Geographie, Geowissenschaften": "Alice.Spinnler@unibas.ch",
                "Germanistik, Nordistik": "susanne.gubser@unibas.ch",
                "Geschichte, allgemein": "David.Trefas@unibas.ch",
                "Geschichte, islamische Länder": "claudia.bolliger@unibas.ch",
                "Geschichte, Osteuropa": "dorothea.trottenberg@unibas.ch",
                "Geschichte, Wirtschaftsgeschichte": "Martin.Luepold@unibas.ch",
                "Griechische und Lateinische Philologie, Humanismus": "christoph.schneider@unibas.ch",
                "Handschriften": "noah.regenass@unibas.ch",
                "Islamwissenschaft, Nahoststudien, Orientalistik": "claudia.bolliger@unibas.ch",
                "Kunstgeschichte": "noah.regenass@unibas.ch",
                "Literaturwissenschaft allg.": "susanne.gubser@unibas.ch",
                "Mathematik, Informatik": "simon.geiger@unibas.ch",
                "Medien, Theater, Tanz": "claudia.bolliger@unibas.ch",
                "Medizin, Pharmazie, Pflegewissenschaften": "monika.wechsler@unibas.ch",
                "Militärwissenschaft": "David.Trefas@unibas.ch",
                "Musikwissenschaft": "iris.lindenmann@unibas.ch",
                "Naturwissenschaften, Technik allg.": "simon.geiger@unibas.ch",
                "Philosophie": "susanne.schaub@unibas.ch",
                "Physik, Astronomie": "simon.geiger@unibas.ch",
                "Politik, Politikwissenschaft": "Uwe.Vonramin@unibas.ch",
                "Psychologie, Pädagogik": "alice.spinnler@unibas.ch",
                "Rechtswissenschaft": "Uwe.Vonramin@unibas.ch",
                "Romanistik": "yvonne.hauser@unibas.ch",
                "Slavistik, Osteuropa-Studien": "dorothea.trottenberg@unibas.ch",
                "Soziologie": "caroline.huwiler@unibas.ch",
                "Sportwissenschaften": "monika.wechsler@unibas.ch",
                "Sprachwissenschaft allg.": "johanna.schuepbach@unibas.ch",
                "Theologie, Religionswissenschaft, Judaistik": "Benedikt.Voegeli@unibas.ch",
                "Wirtschaftswissenschaften": "caroline.huwiler@unibas.ch"}

        return fachgebiete

    def get_mail_to(self, form_values, mode = "debug"):
        if mode in ["prod", "debug"]:
            # Fachgebiet wird schon im Form aufgebereitet
            fachgebiet = form_values.get("fachgebiet")
            return fachgebiet
        else:
            return "TEST Fachreferat <{}>".format(self.mailuser) #TEST_MAIL_TO)

    def get_mail_to_patron(self, form_values, mode = "debug"):
            return self.mailuser

    def get_mail_from_patron(self, form_values, mode = "debug"):
        if mode in ["prod", "debug"]:
            # Fachgebiet wird schon im Form aufgebereitet
            fachgebiet = form_values.get("fachgebiet")
            return fachgebiet
        else:
            return "TEST Fachreferat <{}>".format(self.mailuser)  # TEST_MAIL_TO)

    def get_mail_from(self, form_values, mode = "debug"):
        return self.mailuser

    def get_ticket_msg(self, form_values, mailcontent, mode="debug", mail_from=None, mail_to=None):
        msg = EmailMessage()
        mailcontent = """
 \n\n{}\n\n
Antwort der Bibliothek:
=======================
[ ]  Ist bei uns vorhanden. Signatur:
[ ]  Ist bereits bestellt.
[ ]  Wird angeschafft.
[ ]  Wird für Sie angeschafft. Sie werden benachrichtigt, sobald es
     bereitsteht.
[ ]  Entspricht leider nicht dem Anschaffungsprofil und wird deshalb
     nicht angeschafft.
     \n\nMit freundlichen Grüssen\n\n{}
""".format(mailcontent, self.signatur)
        msg.set_content(mailcontent)
        msg['Subject'] = self.subject or "{}Ticket: {}".format(mode.upper() + " " if mode != "prod" else "" , self.form_title)
        msg['From'] = mail_from or self.get_mail_from(form_values, mode=mode)
        msg['To'] = mail_to or self.get_mail_to(form_values, mode=mode)
        print(msg)
        print(mailcontent)
        return msg


class Fernleihbestellung(FlaskForm, Auth_Form):
    form_title = "Fernleihbestellung"
    specific_parts = {"top": "proj/koplink/iframe_top_ansch.html"}
    subject = "WWW Fernleihe"
    """
      my $AUFTRAGSNUMMER = strftime("%Y.%m.%d-",localtime) .$q->param('uid');
    $Config->{AUFTRAGSNUMMER} = $AUFTRAGSNUMMER;
TITEL = $q->param('tit');
        my $BENUTZERNUMMER = $q->param('uid');
        $mailbody=<<EOD;

Datum: $DATUM
Titel: $TITEL
Abholort: $OTRS
Benutzernummer: $BENUTZERNUMMER

    """

    autor = StringField('AutorIn')
    titel = StringField('Titel', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    verlagsort = StringField('Verlagsort, Verlag')
    erscheinungsjahr = StringField('Erscheinungsjahr')
    serie = StringField('Serie-/Reihenangabe')
    isbn = StringField('ISBN')
    man_kommentar = TextAreaField('Bemerkung, weitere Angaben')

    abholort = SelectField('Abholort', choices=[('ub_hauptbibliothek', 'UB Hauptbibliothek (Ausleihtheke)'), ('ub_wirtschaft', 'UB Wirtschaft - SWA')])

    man_ausland = SelectField('Ich bin einverstanden mit Kosten bis', [validators.DataRequired(message="Feld muss ausgefüllt sein")],
                              choices=[("", "---"),
                                                                               ('chf12', 'CHF 12.- pro Band'),
                                                                               ('chf24', 'CHF 24.- pro Band'),
                                                                               ('chf36', 'CHF 36.- pro Band')],
                              default="")

    man_lieferart = SelectField('Version', choices=[('norelevance', 'keine Relevanz'),
                                                    ('auflage', 'Nur diese Auflage'),
                                                    ('sprache', 'Nur diese Sprache')], default="norelevance")

    def get_mail_to(self, form_values, mode="debug"):
        if mode in ["prod", "debug"]:
            return "UB Info <info-ub@unibas.ch>"
        else:
            return f"TEST UB Info  <{self.mailuser}>"

    def get_mail_from(self, form_values, mode="debug"):
        return self.mailuser

    def get_mail_to_patron(self, form_values, mode="debug"):
        return self.mailuser

    def get_mail_from_patron(self, form_values, mode="debug"):
        if mode in ["prod", "debug"]:
            return "UB Info <info-ub@unibas.ch>"
        else:
            return "TEST UB Info  <info-ub@unibas.ch>"


class FernleihbestellungTest(Fernleihbestellung):
    form_title = "Fernleihbestellung Test"

# import MyBibTemplate before AuthForm otherwise methods are not overwritten
class MyBibSFXTemplate(MyBibTemplate, Auth_Form):


    counter_url = "https://intraweb.ub.unibas.ch/php/admin/counter.php?next="

    # Values werden für Kopien LinkResolver Formularautomatisch aus der OpenUrl ausgelesen -> Variable-Name fungiert als Key
    Article = StringField('Titel des Artikels/Beitrags', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    Journal = StringField('Zeitschriften- / Buchtitel', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    Author = StringField('AutorIn')
    Year = StringField('Jahr')
    Volume = StringField('Volume')
    Issue = StringField('Issue')
    Pages = StringField('Seiten')
    ISSN = StringField('ISSN')

    def build_mybib_format(self, form_values):
        # kommentarlabels nicht ganz korrekt, eigentlich Bemerkung, Source, PMID/AN
        # kein check, ob ISSN mit iSBN: anfängt
        backup_path = "{}.json".format(os.path.join(self.backup_dir,form_values.get("Auftragsnummer"))).replace(" ", "-")
        print(backup_path)
        json.dump(form_values, open(backup_path, "w"))
        mybib = """typ(FES)
subtyp(FES_ZEITSCHRIFT)
version(1.2)
auftragsnummer($Auftragsnummer)
bestelldatum($Bestelldatum)
lieferart($man_lieferart)
lieferformat(PDF)
liefertyp(COPY)
lieferzusatz($man_ausland)
artikel-gesamt-titel($Journal)
artikel-titel($Article)
artikel-autor($Author)
artikel-erscheinungsjahr($Year)
artikel-erscheinungsort()
artikel-seitenangabe($Pages)
artikel-issn($ISSN)
artikel-isbn($ISBN)
artikel-ausgabe(Volume: $Volume - Issue: $Issue)
artikel-band($Volume)
artikel-heft($Issue)
artikel-referenz-quelle($sfxurl)
mediennummer($meduid)
auftraggeber-name($slsp_full_name)
besteller-name($slsp_full_name)
besteller-kundennummer($slsp_id_hidden)
besteller-institution($slsp_institution)
besteller-strasse($slsp_adresse)
besteller-stadt($slsp_plz)
kontakt-telefon($slsp_phone)
besteller-email($slsp_mail)
bestellsystem-kommentar($merged_Kommentar)
bestellsystem-gruppe($slsp_usergroup_hidden)
bestellsystem-gruppe-beschreibung($slsp_usergroup_desc_hidden)"""

        mybib_result = Template(mybib).safe_substitute(**form_values)
        mybib_result = re.sub("\$[^ \(\)]*", "", mybib_result)
        return mybib_result

class Kopienbestellung_LinkResolverSFX_Base(FlaskForm, MyBibSFXTemplate):
    form_title = "Kopienbestellung SFX LinkResolver"
    nc_prefix = "SFX"
    specific_parts = {"top": "proj/koplink/iframe_top.html"}
    settings = {"MybibDomain": "BS Test", "MybibEmail": "martin.reisacher@unibas.ch",
                "EdocMail": "martin.reisacher@unibas.ch", "LibraryName": "UB Basel (TEST)",
                "Counter": "SFX_DOD_TEST", "LibCode": "A100"}
    signatur = """UNIVERSITÄT BASEL | Universitätsbibliothek | Information und Fernleihe
Schönbeinstrasse 18-20 | 4056 Basel | Schweiz        
Tel +41 61 207 31 00 | Fax +41 61 207 31 03
E-Mail info-ub@unibas.ch | www.ub.unibas.ch"""

    mail_from = "UB Info <info-ub@unibas.ch>"

    # Values werden automatisch aus der OpenUrl ausgelesen -> Variable-Name fungiert als Key
    ISBN = StringField('ISBN')
    meduid = StringField('Meduid')
    Source = StringField('Quelle')
    sfxurl = StringField('Document Uri')

    man_kommentar = TextAreaField('Bemerkungen, weitere Angaben', render_kw={'class': 'text-field'})
    man_lieferart = RadioField('Versandart', choices=[('WEB', 'Elektronisch (wenn möglich)*'),
                                                ('POST', 'Postversand')], default="WEB")

class Kopienbestellung_LinkResolverSFX_Test(Kopienbestellung_LinkResolverSFX_Base):
    form_title = "Kopienbestellung SFX LinkResolver (MyBib/Counter Testsettings)"
    settings = {"MybibDomain": "BS Test", "MybibEmail": "martin.reisacher@unibas.ch",
                "EdocMail": "martin.reisacher@unibas.ch", "LibraryName": "UB Basel (TEST)",
                "Counter": "SFX_DOD_TEST", "LibCode": "A100"}

    def get_mail_to(self, form_values, mode = "debug"):
        return "MyBIB <{}>".format(self.mailuser) #TEST_MAIL_TO)

class Kopienbestellung_LinkResolverSFX_UB(Kopienbestellung_LinkResolverSFX_Base):
    form_title = "Kopienbestellung SFX LinkResolver UB"
    settings = {"MybibDomain": "BS UB", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "info-ub@unibas.ch", "LibraryName": "Universitätsbibliothek Basel",
                "Counter": "SFX_DOD_UB", "LibCode": "A100"}


class Kopienbestellung_LinkResolverSFX_UBM(Kopienbestellung_LinkResolverSFX_Base):
    form_title = "Kopienbestellung SFX LinkResolver UBM"
    settings = {"MybibDomain": "BS Medbib", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "info-medb@unibas.ch", "LibraryName": "UB Medizin, Basel",
                "Counter": "SFXILL", "LibCode": "A140"}
    signatur = """UNIVERSITÄT BASEL
UB Medizin
Information und Fernleihe
info-medb@unibas.ch
Tel +41 61 207 32 00"""

class Kopienbestellung_LinkResolverSFX_UBW(Kopienbestellung_LinkResolverSFX_Base):
    form_title = "Kopienbestellung SFX LinkResolver UBW"
    settings = {"MybibDomain": "BS WWZ", "MybibEmail": MAIL_MYBIBTO,
                "EdocMail": "kopien-ubw-swa@unibas.ch", "LibraryName": "UB Wirtschaft - SWA, Basel",
                "Counter": "SFX_DOD_WWZ", "LibCode": "A125"}
    signatur = """UNIVERSITÄT BASEL
UB Wirtschaft - SWA
Information und Fernleihe
ill-ubw-swa@unibas.ch"""

class Zettelkatalog(FlaskForm, Auth_Form):
    specific_parts = {"bottom": "proj/koplink/img_zettelkatalog.html"}


    form_title = "Dissertationenkatalog bis 1980"
    subject = "Dissertationenkatalog bis 1980"

    bildid = StringField('Bild ID', [validators.DataRequired(message="Feld muss ausgefüllt sein")])
    man_kommentar = TextAreaField('Bemerkung, weitere Angaben')

    man_ausland = SelectField('Ich bin einverstanden mit Kosten bis', [validators.DataRequired(message="Feld muss ausgefüllt sein")],
                              choices=[("", "---"),
                                                                               ('chf12', 'CHF 12.- pro Band'),
                                                                               ('chf24', 'CHF 24.- pro Band'),
                                                                               ('chf36', 'CHF 36.- pro Band')],
                              default="")

    man_lieferart = SelectField('Version', choices=[('norelevance', 'keine Relevanz'),
                                                    ('auflage', 'Nur diese Auflage'),
                                                    ('sprache', 'Nur diese Sprache')], default="norelevance")

    def get_mail_to(self, form_values, mode="debug"):
        if mode in ["prod", "debug"]:
            return "UB Info <info-ub@unibas.ch>"
        else:
            return "TEST UB Info <info-ub@unibas.ch>"

    def get_mail_from(self, form_values, mode="debug"):
        return self.mailuser

    def get_mail_to_patron(self, form_values, mode="debug"):
        return self.mailuser

    def get_mail_from_patron(self, form_values, mode="debug"):
        if mode in ["prod", "debug"]:
            return "UB Info <info-ub@unibas.ch>"
        else:
            return "TEST UB Info  <info-ub@unibas.ch>"