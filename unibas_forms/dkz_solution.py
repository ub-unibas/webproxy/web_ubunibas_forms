from flask import Flask, render_template, request, flash, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, RadioField, HiddenField, StringField, IntegerField, FloatField, validators
from wtforms.validators import InputRequired, Length, Regexp, NumberRange
from wtforms_sqlalchemy.fields import QuerySelectField
from datetime import date, datetime

class DigiIDFinderForm(FlaskForm):
    specific_parts = {}
    digi_id = StringField("Systemnummer / Alma-ID / Item-ID", [validators.DataRequired("Es muss eine ID angegeben werden")])
    title = HiddenField("Titel")
    alma_id = HiddenField("Alma-IZ-ID")
    sys_id = HiddenField("Systemnummer")
    other_ids = HiddenField("Andere IDs")
    error = HiddenField("Fehler")

    submit = SubmitField('Suchen')
    confirm = SubmitField('Suchen')
    back = SubmitField('Zurück')
    create_nc = SubmitField('Zurück')
    reload = SubmitField('Reload')
    new = SubmitField('Neues Formular')