from gdspreadsheets_ubit import PygExtendedClient,authorize_pyg

service_file = "/home/martin/PycharmProjects/web_ubunibas_content/personenzaehlanlage-79993bb25b5c.json"
spreadsheet_id = "1FYIev4QsW_3q_VOtuAzJG4sUCYq0R9QI0khAxXzKlmQ"
sheet = "Anschaffungsvorschlag"
key_field = "Fach"

PygExtendedClient.gc = authorize_pyg(gd_service_account_env_var='GOOGLE_DOC_API_KEY',
                                     service_file=service_file)
spreadsheet = PygExtendedClient.gc.open_by_key(spreadsheet_id)
max_sheet =spreadsheet.worksheet_by_title(sheet)
print({k:v["Mailadresse"] for k,v in max_sheet.gSpreadsheetKeyDict(key_field).items()})