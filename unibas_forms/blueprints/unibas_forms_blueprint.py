from flask import Blueprint, request, render_template, redirect
from flask_login import login_required, current_user
import os

MODE = os.getenv('MODE') or "debug"
backup_dir = "/tmp"
from unibas_forms.unibas_forms_logic import Anschaffungsvorschlag, Kopienbestellung, KopienbestellungTest, \
    Kopienbestellung_LinkResolver_UB, Kopienbestellung_LinkResolver_UBM, Kopienbestellung_LinkResolver_UBW, \
    Kopienbestellung_LinkResolver_Test, Fernleihbestellung, FernleihbestellungTest, \
    Kopienbestellung_LinkResolverSFX_UBM, Kopienbestellung_LinkResolverSFX_UBW, Kopienbestellung_LinkResolverSFX_UB, \
    Kopienbestellung_LinkResolverSFX_Test, Zettelkatalog

unibas_forms_blue = Blueprint('unibas_forms', __name__)

@unibas_forms_blue.route('/static/<file>', methods=['GET', 'POST'])
def serve_static(file):
    return unibas_forms_blue.send_static_file(file)

@unibas_forms_blue.route('/form_kop/header', methods=['GET', 'POST'], defaults={"header": True})
@unibas_forms_blue.route('/form_kop', methods=['GET', 'POST'], defaults={"header": False})
@login_required
def form_kopienbestellung(header):
    user = current_user.get_id()

    if MODE == "prod":
        return Kopienbestellung.form_auth_template(request, user, mode=MODE, header=header, backup_dir=backup_dir)
    else:
        return KopienbestellungTest.form_auth_template(request, user, mode=MODE, header=header, backup_dir=backup_dir)

@unibas_forms_blue.route('/form_fernleihe/header', methods=['GET', 'POST'], defaults={"header": True})
@unibas_forms_blue.route('/form_fernleihe', methods=['GET', 'POST'], defaults={"header": False})
@login_required
def form_fernleihe(header):
    user = current_user.get_id()

    if MODE == "prod":
        return Fernleihbestellung.form_auth_template(request, user, mode=MODE, header=header, backup_dir=backup_dir)
    else:
        return FernleihbestellungTest.form_auth_template(request, user, mode=MODE, header=header, backup_dir=backup_dir)





@unibas_forms_blue.route('/form_ansch/header', methods=['GET', 'POST'], defaults={"header": True})
@unibas_forms_blue.route('/form_ansch', methods=['GET', 'POST'], defaults={"header": False})
@login_required
def form_ansch(header):
    user = current_user.get_id()
    if MODE == "prod":
        return Anschaffungsvorschlag.form_auth_template(request, user, mode=MODE, header=header)
    else:
        return Anschaffungsvorschlag.form_auth_template(request, user, mode=MODE, header=header)

@unibas_forms_blue.route('/form_zettelkatalog/header', methods=['GET', 'POST'], defaults={"header": True})
@unibas_forms_blue.route('/form_zettelkatalog', methods=['GET', 'POST'], defaults={"header": False})
#@login_required
def form_zettelkatalog(header):
    user = current_user.get_id()

    return Zettelkatalog.form_auth_template(request, user, mode=MODE, header=header)

@unibas_forms_blue.route('/form_koplink/header', methods=['GET', 'POST'], defaults={"header": True})
@unibas_forms_blue.route('/form_koplink', methods=['GET', 'POST'], defaults={"header": False})
@login_required
def form_kopienbestellung_linkresolver(header):
    user = current_user.get_id()
    source = request.args.get("source")

    if MODE != "prod":
        return Kopienbestellung_LinkResolver_Test.form_auth_template(request, user, remove_empty_fields=False, header=header, mode=MODE, backup_dir=backup_dir)
    #chuv-bium: Centre hospitalier universitaire vaudois
    elif source in {'EBSCO:cin20', 'EBSCO:cmedm', 'EBSCO:sph', 'OVID:psyndb', 'OVID:psycdb', 'OVID:pscrdb',
                  'OVID:medline', 'Entrez:PubMed', 'BMC:F1000', 'sfxit.com:citation', 'CAS:MEDLINE',
                  'CSA:medline-set-c', 'CAS:CAPLUS', 'Elsevier:SD', 'EMBASE', 'google', 'drdoc', 'Doctor-Doc', 'chuv-bium'}:
        return Kopienbestellung_LinkResolver_UBM.form_auth_template(request, user, remove_empty_fields=False, header=header, mode=MODE, backup_dir=backup_dir)
    elif source in {'EBSCO:buh', 'EBSCO:ecn', 'CSA:socioabs-set-c', 'CSA:pais-set-c', 'CSA:paisarc-set-c', 'GBI:wiwi',
                    'GBI:wiso1', 'GBI:wiso2', 'GBI:wiso3'}:
        return Kopienbestellung_LinkResolver_UBW.form_auth_template(request, user, remove_empty_fields=False, header=header, mode=MODE, backup_dir=backup_dir)
    else:
        return Kopienbestellung_LinkResolver_UB.form_auth_template(request, user, remove_empty_fields=False, header=header, mode=MODE, backup_dir=backup_dir)

@unibas_forms_blue.route('/form_sfxkoplink/header', methods=['GET', 'POST'], defaults={"header": True})
@unibas_forms_blue.route('/form_sfxkoplink', methods=['GET', 'POST'], defaults={"header": False})
@login_required
def form_kopienbestellung_sfxlinkresolver(header):
    user = current_user.get_id()
    source = request.args.get("Source","").replace(" (Via SFX)","")

    if MODE != "prod":
        return Kopienbestellung_LinkResolverSFX_Test.form_auth_template(request, user, remove_empty_fields=True, header=header, mode=MODE, backup_dir=backup_dir)
    elif source in {'drdoc', 'EBSCO:cin20', 'EBSCO:cmedm', 'EBSCO:sph', 'OVID:psyndb', 'OVID:psycdb', 'OVID:pscrdb',
                  'OVID:medline', 'Entrez:PubMed', 'BMC:F1000', 'sfxit.com:citation', 'CAS:MEDLINE',
                  'CSA:medline-set-c', 'CAS:CAPLUS', 'Elsevier:SD', 'EMBASE', 'google'}:
        return Kopienbestellung_LinkResolverSFX_UBM.form_auth_template(request, user, remove_empty_fields=True, header=header, mode=MODE, backup_dir=backup_dir)
    elif source in {'EBSCO:buh', 'EBSCO:ecn', 'CSA:socioabs-set-c', 'CSA:pais-set-c', 'CSA:paisarc-set-c', 'GBI:wiwi',
                    'GBI:wiso1', 'GBI:wiso2', 'GBI:wiso3'}:
        return Kopienbestellung_LinkResolverSFX_UBW.form_auth_template(request, user, remove_empty_fields=True, header=header, mode=MODE, backup_dir=backup_dir)
    else:
        return Kopienbestellung_LinkResolverSFX_UB.form_auth_template(request, user, remove_empty_fields=True, header=header, mode=MODE, backup_dir=backup_dir)


def increment_counter(prefix_choice):
    from unibas_forms import auth_db
    from unibas_forms.unibas_counter import Counter
    nc = Counter.query.filter_by(prefix=prefix_choice).first()
    nc.nc = nc.nc + 1
    auth_db.session.commit()
    return nc.nc

@unibas_forms_blue.route('/nc/<library>', methods=['GET', 'POST'])
@login_required
def add_record(library):
    from unibas_forms.unibas_counter import Counter, CounterForm, CounterMyBib
    from unibas_forms import auth_db
    form_counter = CounterForm(request.form)
    user = current_user.get_id()

    if not user:
        eduid_service = "{}{}".format(request.url_root, "login")
        user = {}
    else:
        eduid_service = None
        user = user
        form_counter.mailuser = user.get("mail", [""])[0]
        from unibas_forms import Auth_Form
        staff = Auth_Form.get_alma_user(form_counter.mailuser)
    form_counter.lib = library
    libraries = set([entry.library for entry in auth_db.session.query(Counter).all()])
    prefix_entries = [entry.prefix for entry in auth_db.session.query(Counter).filter_by(library=library).all()]

    if user and request.method == 'POST' and form_counter.validate() and (form_counter.submit.data or form_counter.confirm.data):
        prefix_choice = request.form['prefix']
        new_counter = increment_counter(prefix_choice)
        form_counter.counter.data = new_counter
        signatur = "{} {}".format(prefix_choice, new_counter)
        form_counter.signatur.data = signatur
        #form_counter.process(formdata={}, obj={}, **{"prefix": prefix_choice, "counter": new_counter, "signatur": signatur})
        return render_template("auth_form_counter.html", title="Numerus Currens",
                               form=form_counter, eduid_service=eduid_service, user=user,
                               remove_empty_fields=False, status="confirm", header=True,
                                   specific_parts=form_counter.specific_parts)
    elif form_counter.back.data:
        return redirect(request.script_root + request.path)
    else:
        if form_counter.create_nc.data:
            return render_template("auth_form_counter.html", title="Numerus Currens",
                        form=form_counter, user=user,
                        remove_empty_fields=False, status="create", header=True, prefix_entries=prefix_entries,
                                   specific_parts=form_counter.specific_parts)
        else:
            return render_template("auth_form_counter.html", title="Numerus Currens",
                        form=form_counter, eduid_service=eduid_service, user=user,
                        remove_empty_fields=False, status="fillout", header=True, prefix_entries=prefix_entries,
                                   specific_parts=form_counter.specific_parts)

from unibas_forms.dkz_solution import DigiIDFinderForm
@unibas_forms_blue.route('/digi_id', methods=['GET', 'POST'])
def get_digi_id():
    try:

        from rdv_marc_ubit import AlmaIZMarcJSONRecord
        digi_id_form = DigiIDFinderForm(request.form)
        user = current_user.get_id()

        if 0:
            # auth einstweilen ausgeschalten
            if not user:

                eduid_service = None # "{}{}".format(request.url_root, "login")
                user = {}
            else:
                eduid_service = None
                user = user

                digi_id_form.mailuser = user.get("mail", [""])[0]
                from unibas_forms import Auth_Form
                staff = Auth_Form.get_alma_user(digi_id_form.mailuser)

        # uth einstweilen ausgeschalten: user and
        if request.method == 'POST' and digi_id_form.validate() and (digi_id_form.submit.data or digi_id_form.confirm.data):
            digi_id = request.form['digi_id']
            try:
                al = AlmaIZMarcJSONRecord.get_sysid(digi_id)
                alma_id = al.get_main_id()[0]
                sys_id = al.get_aleph_sysid()
                title = al.get_title()[0]
                other_ids = al.get_field_data(marc_fields=["035"], subfields=["a"])
                digi_id_form.process(formdata=request.form, obj={}, **{"alma_id": alma_id, "sys_id": sys_id, "other_ids": other_ids, "title": title})
            except TypeError:
                error = "ID {} konnte nicht gefunden werden".format(digi_id)
                digi_id_form.process(formdata=request.form, obj={}, **{"error": error})

            return render_template("digi_form_temp.html", title="Digi ID Finder",
                                   form=digi_id_form, user=user,
                                   remove_empty_fields=False, status="confirm", header=True,
                                   specific_parts=digi_id_form.specific_parts)
        elif digi_id_form.back.data:
            return redirect(request.script_root + request.path)
        else:
            return render_template("digi_form_temp.html", title="Digi ID Finder",
                                   form=digi_id_form, user=user,
                                   remove_empty_fields=False, status="fillout", header=True, prefix_entries={},
                                   specific_parts=digi_id_form.specific_parts)
    except Exception:
        import traceback
        traceback.print_exc()
        return render_template(template_name_or_list="message_template.html", message="Problem beim Nachschlagen der ID. Bitte wenden Sie sich an die IT", header=True,
                               error=True, specific_parts=DigiIDFinderForm.specific_parts)