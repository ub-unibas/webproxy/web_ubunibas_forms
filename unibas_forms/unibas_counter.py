from flask import Flask, render_template, request, flash, redirect
from flask_sqlalchemy import SQLAlchemy
from flask_wtf import FlaskForm
from wtforms import SubmitField, SelectField, RadioField, HiddenField, StringField, IntegerField, FloatField, validators
from wtforms.validators import InputRequired, Length, Regexp, NumberRange
from wtforms_sqlalchemy.fields import QuerySelectField
from datetime import date, datetime

from unibas_forms import auth_db

# required integer


# each table in the database needs a class to be created for it
# db.Model is required - don't change it
# identify all columns by name and data type
class Counter(auth_db.Model):
    __tablename__ = 'nc_counter'
    __bind_key__ = 'counter'
    #id = db.Column(db.Integer, primary_key=True)
    id = auth_db.Column(auth_db.Integer, primary_key=True)
    library = auth_db.Column(auth_db.String(20))
    prefix = auth_db.Column(auth_db.String (224), primary_key=True)
    nc = auth_db.Column(auth_db.Integer)
    date = auth_db.Column(auth_db.DateTime, nullable=False,
                          default=datetime.utcnow)

    def __init__(self, prefix, nc, library, date):
        self.prefix = prefix
        self.nc = nc
        self.library = library
        self.date = date

class WWZsign(auth_db.Model):
    __tablename__ = 'wwzsignatur'
    __bind_key__ = 'counter'
    prefix = auth_db.Column(auth_db.String (224), primary_key=True)
    nc = auth_db.Column(auth_db.Integer)
    date = auth_db.Column(auth_db.TIMESTAMP, nullable=False,
                          default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, prefix, nc, date):
        self.prefix = prefix
        self.nc = nc
        self.date = date

class CounterMyBib(auth_db.Model):
    __tablename__ = 'counter'
    __bind_key__ = 'counter'
    id = auth_db.Column(auth_db.Integer, primary_key=True)
    prefix = auth_db.Column(auth_db.String(50), primary_key=True)
    nc = auth_db.Column(auth_db.Integer)
    user = auth_db.Column(auth_db.String)
    date = auth_db.Column(auth_db.DateTime, nullable=False,
                          default=datetime.utcnow)

    def __init__(self, prefix, counter, user, date):
        self.prefix = prefix
        self.counter = counter
        self.user = user
        self.date = date

from sqlalchemy import func
def get_prefixes(form, field):
    prefix_entries = [entry.prefix for entry in auth_db.session.query(Counter).filter_by(library=form.lib).all()]
    if field.data not in prefix_entries and form.create_nc.data is False:
        raise validators.ValidationError(
            'Prefix existiert nicht. Bitte neu anlegen')

def get_number(form, field):
    import re
    if re.match("\d*", field.data) and form.create_nc.data is True:
        raise validators.ValidationError(
            'Feld muss eine Zahl sein')

class CounterForm(FlaskForm):
    specific_parts = {}
    # id used only by update/edit
    #, query_factory=enabled_categories, get_label="prefix"
    prefix = StringField("Präfix", [validators.DataRequired(message="Feld muss ausgefüllt sein"),
                                  get_prefixes])

    counter = StringField("Numerus Currens", [get_number], )
    signatur = HiddenField("Signatur")

    submit = SubmitField('Neue NC Signatur')
    confirm = SubmitField('Weitere NC Signatur')
    create_nc = SubmitField('Neuen Numerus Currens anlegen')
    #create_k = SubmitField('Neuen Numerus Currens anlegen')
    reload = SubmitField('Reload')
    back = SubmitField('Zurück')






