
import os
app_config_prod = {"OIC_PROVIDER": "https://login.eduid.ch/",
              "OIDC_REDIRECT_URIS": ["https://ub-webform.ub.unibas.ch/login/oidc"],
              "CLIENT_ID": "ub-unibas-webform",
              # "OIC_SCOPE": ["openid", "profile", "email", "swissEduIDBase", "swissEduIDExtended"],
              "OIC_SCOPE": ["openid", "profile", "email", "https://login.eduid.ch/authz/User.Read"],
              "SQLALCHEMY_DATABASE_URI": os.getenv("AUTH_DB"),
               "SQLALCHEMY_BINDS": {
                    'counter':        os.getenv("COUNTER_BIND"),
               },
              "SQLALCHEMY_TRACK_MODIFICATIONS": False,
              "SECRET_KEY": os.getenv("SECRET_KEY").strip(),
              "ALMA_API_KEY": os.getenv("ALMA_API_KEY").strip(),
              "ALMA_API_SANDBOX_KEY": os.getenv("ALMA_API_SANDBOX_KEY").strip(),
              "ALMA_SANDBOX": False,
              "PRIV_KEY_JWT": os.getenv("PRIV_KEY_JWT").strip(),
              "GD_SERVICE_FILE": os.getenv("GD_SERVICE_FILE"),
              "DEBUG": False}

app_config_test = {"OIC_PROVIDER": "https://login.eduid.ch/",
              "OIDC_REDIRECT_URIS": ["https://ub-test-webform.ub.unibas.ch/login/oidc"],
              "CLIENT_ID": "ub-unibas-webform",
              #"OIC_SCOPE": ["openid", "swissEduIDBase"],
              "OIC_SCOPE": ["openid", "profile", "email", "https://login.eduid.ch/authz/User.Read"],
                   "SQLALCHEMY_DATABASE_URI": os.getenv("AUTH_DB"),
               "SQLALCHEMY_BINDS": {
                        'counter':  os.getenv("COUNTER_BIND"),
                                    },
              "SQLALCHEMY_TRACK_MODIFICATIONS": False,
                   "SECRET_KEY": os.getenv("SECRET_KEY").strip(),
                   "ALMA_API_KEY": os.getenv("ALMA_API_KEY").strip(),
                   "ALMA_API_SANDBOX_KEY": os.getenv("ALMA_API_SANDBOX_KEY").strip(),
              "ALMA_SANDBOX": False,
                   "PRIV_KEY_JWT": os.getenv("PRIV_KEY_JWT").strip(),
                   "GD_SERVICE_FILE": os.getenv("GD_SERVICE_FILE"),
              "DEBUG": False}

