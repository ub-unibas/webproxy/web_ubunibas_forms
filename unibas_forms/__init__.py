import os
import socket
import pickle

from flask import Flask
from flask_login import LoginManager

from ubunibas_auth import AuthUserObject, auth_db
from ubunibas_slsp_auth import ubunibas_slsp_auth
from unibas_forms.blueprints.unibas_forms_blueprint import unibas_forms_blue
from unibas_forms.app_config import app_config_prod, app_config_test
# from unibas_forms.app_config_debug import app_config_debug
app_config_debug = {}

MODE = os.getenv('MODE') or "debug"
print("MODE", MODE)
unibas_form_app = Flask(__name__, template_folder='templates', static_url_path="", static_folder='static')

if socket.gethostname() in ["reisache"]:
    MODE = "debug"
    unibas_form_app.config.update(app_config_debug)
    backup_dir = "/tmp"
elif MODE == "test":
    unibas_form_app.config.update(app_config_test)
    backup_dir = "/home/web_services/mybib_archive/test"
elif MODE == "prod":
    unibas_form_app.config.update(app_config_prod)
    backup_dir = "/backup"


unibas_form_app.register_blueprint(ubunibas_slsp_auth)
unibas_form_app.register_blueprint(unibas_forms_blue)
auth_db.init_app(unibas_form_app)
with unibas_form_app.app_context():
    auth_db.create_all()
    auth_db.session.commit()
login_manager = LoginManager()
login_manager.init_app(unibas_form_app)
login_manager.login_view = "ubunibas_slsp_auth.login"

#Why?
@unibas_form_app.template_filter()
def leading_zeros(value):
    try:
        value = int(value)
    except:
        value = 0
    return "{:08d}".format(value)


@login_manager.user_loader
def user_loader(uid):
    auth = AuthUserObject.query.get(uid)
    if auth:
        unpickeled_auth = AuthUserObject(auth.id, pickle.loads(auth.userinfo), pickle.loads(auth.role), auth.email)
        return unpickeled_auth
    else:
        return None