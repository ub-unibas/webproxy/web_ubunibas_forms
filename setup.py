#!/usr/bin/python3

import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="web_ubunibas_forms", # Replace with your own username
    setup_requires=['setuptools-git-versioning'], # 0.4.11
    setuptools_git_versioning={
        "enabled": True,
    },
    author="Martin Reisacher",
    author_email="martin.reisacher@unibas.ch",
    description="complex webforms for ub.unibas.ch",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    #package_dir={"": "ubunibas_loggers"},
    packages=setuptools.find_packages(),
    install_requires=['requests', 'pymysql', 'flask', 'flask_sqlalchemy', 'flask-restx', 'flask-login', 'flask_wtf',
                      'wtforms', 'flask_cors', 'flask_compress',
                      'flask_oauthlib', 'flask-pyoidc', 'feedparser', 'uwsgi', 'netaddr', 'openpyxl', 'mysql-connector',
                      'cache_decorator_redis_ubit', 'gdspreadsheets_ubit', 'web_ubunibas_auth', 'jwt', 'mysqlclient', "rdv_marc_ubit", "wtforms-sqlalchemy"],
    python_requires=">=3.8",
    include_package_data=True,
    zip_safe=False
)