import socket
HOSTNAME = socket.gethostname()

if __name__ == "__main__":
    from unibas_forms import unibas_form_app
    unibas_form_app.run(debug=True, ssl_context='adhoc', port=5050)